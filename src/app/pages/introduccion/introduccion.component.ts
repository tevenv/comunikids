import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-introduccion',
  templateUrl: './introduccion.component.html',
  styleUrls: ['./introduccion.component.scss'],
})
export class IntroduccionComponent implements OnInit {

  

  constructor() { }

  ngOnInit() {}

  contenido:string = "Comunikids, queremos acompañarte con esta aplicación móvil, para que cada día se convierta en una oportunidad de aprendizaje significativo para tu vida. Con la lectura de cuentos y las actividades podrás disfrutar el placer de conocer otros mundos y realidades, reconocer y continuar con el desarrollo de tus competencias comunicativas,  a través de la expresión oral, así como la comprensión de lo que lees y observas en tu entorno.";
  contenido2:string = "Estamos seguros que este es un recurso importante que, junto con tu esfuerzo, las explicaciones y el apoyo de tus padres, contribuirán a fortalecer tus aprendizajes para crear y expresar las ideas, emociones y sensaciones acerca de lo que te rodea";

  

}
