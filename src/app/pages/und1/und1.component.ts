import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-und1',
  templateUrl: './und1.component.html',
  styleUrls: ['./und1.component.scss'],
})
export class Und1Component implements OnInit {

  
  constructor(private router:Router) { 
    
  }

  ngOnInit() {}

  public titulo1:string = "Objetivo del Aprendizaje ";
  public titulo2:string = "Justificación";
  public titulo3:string = "Actividad ";

  public contenido1:string = "Reconocer la estructura de un texto y contarlo con sus propias palabras siguiendo la secuencia de la historia.";
  public contenido2:string = "Mediante la realización de estas actividades significativas el niño se mostrará más motivado en la narración de cuentos, despertá el interés por la literatura, acogiendo este como un hábito no solo a nivel institucional sino también en su familia.";
  public contenido3:string = "Vamos a leer el cuento “el pez llorón” luego responder las preguntas.";

  iniactividad(){
    this.router.navigateByUrl('/actividad/uni1');
  }


}
