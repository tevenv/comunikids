import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Und1Component } from './und1.component';

describe('Und1Component', () => {
  let component: Und1Component;
  let fixture: ComponentFixture<Und1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Und1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Und1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
