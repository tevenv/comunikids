import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-objetivos',
  templateUrl: './objetivos.component.html',
  styleUrls: ['./objetivos.component.scss'],
})
export class ObjetivosComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  Contenido1:string="Implementar diferentes estrategias didácticas para el mejoramiento de la comprensión lectora en los niños y niñas de segundo grado de la Institución Educativa Jorge Eliecer Gaitán Ayala, sede Atanasio Girardot, donde pueda analizar textos escritos, extrayendo la idea principal, así como enriquecer su vocabulario, favoreciendo la expresión, la comprensión oral y la expresión escrita.";
}
