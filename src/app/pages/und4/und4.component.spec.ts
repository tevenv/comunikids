import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Und4Component } from './und4.component';

describe('Und4Component', () => {
  let component: Und4Component;
  let fixture: ComponentFixture<Und4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Und4Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Und4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
