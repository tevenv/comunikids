import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-und4',
  templateUrl: './und4.component.html',
  styleUrls: ['./und4.component.scss'],
})
export class Und4Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {}

  public titulo1:string = "Objetivo del Aprendizaje ";
  public titulo2:string = "Justificación";
  public titulo3:string = "Actividad ";

  public contenido1:string = "El estudiante comprenda, interprete y analice diferentes clases de texto, avanzando en la comprensión de las narraciones leídas, extrayendo información explícita e implícita; reconstruyendo la secuencia de las acciones en la historia; expresando opiniones fundamentadas sobre hechos y situaciones del texto.";
  public contenido2:string = "A través de esta secuencia didáctica se espera que los estudiantes desarrollen sus habilidades de comprensión e interpretación de textos orales y escritos; para ello, se parte de preguntas o situaciones de interés, las cuales conllevan cierta reflexión. De esta forma, podrá ponerse a prueba la comprensión y atención del estudiante.";
  public contenido3:string = "Lee con atención el texto descriptivo “El aguacate”. ";

  iniactividad(){
    this.router.navigateByUrl('/actividad/uni4');
  }

}
