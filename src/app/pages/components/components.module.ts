import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from '../inicio/inicio.component';
import { IonicModule } from '@ionic/angular';
import { IntroduccionComponent } from '../introduccion/introduccion.component';
import { ObjetivosComponent } from '../objetivos/objetivos.component';
import { Und1Component } from '../und1/und1.component';
import { Und2Component } from '../und2/und2.component';
import { Und3Component } from '../und3/und3.component';
import { Und4Component } from '../und4/und4.component';







@NgModule({
  declarations: [
    InicioComponent,
    IntroduccionComponent,
    ObjetivosComponent,
    Und1Component,
    Und2Component,
    Und3Component,
    Und4Component
  ],
  exports:[
    InicioComponent,
    IntroduccionComponent,
    ObjetivosComponent,
    Und1Component,
    Und2Component,
    Und3Component,
    Und4Component
  ],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
