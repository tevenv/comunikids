import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-und2',
  templateUrl: './und2.component.html',
  styleUrls: ['./und2.component.scss'],
})
export class Und2Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {}

  public titulo1:string = "Objetivo del Aprendizaje ";
  public titulo2:string = "Justificación";
  public titulo3:string = "Actividad ";

  public contenido1:string = "Favorecer el interés por la lectura, a través de actividades significativas donde el estudiante pueda identificar la idea principal de un texto.";
  public contenido2:string = "Se pretende que los estudiantes se diviertan, participen y favorecer la fluidez verbal, atención, memoria; así adquieran habilidades comunicativas para la vida.";
  public contenido3:string = "Vamos a leer el cuento del gato Vladimir, luego responde el cuestionario.";

  iniactividad(){
    this.router.navigateByUrl('/actividad/uni2');
  }

}
