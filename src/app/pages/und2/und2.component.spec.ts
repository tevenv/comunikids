import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Und2Component } from './und2.component';

describe('Und2Component', () => {
  let component: Und2Component;
  let fixture: ComponentFixture<Und2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Und2Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Und2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
