import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-und3',
  templateUrl: './und3.component.html',
  styleUrls: ['./und3.component.scss'],
})
export class Und3Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {}

  public titulo1:string = "Objetivo del Aprendizaje ";
  public titulo2:string = "Justificación";
  public titulo3:string = "Actividad ";

  public contenido1:string = "El estudiante identifica las partes de una fábula.";
  public contenido2:string = "Se pretende que el niño se divierta, participe y potencialice sus habilidades y a la vez enriquezca su creatividad, fortaleciendo valores y la expresión de los sentimientos y emociones; mejorando así la autoestima de los niños y niñas.";
  public contenido3:string = "Vamos a leer la fábula “la gallina y el diamante”, luego responder las preguntas.";

  iniactividad(){
    this.router.navigateByUrl('/actividad/uni3');
  }
}
