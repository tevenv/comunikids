import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Und3Component } from './und3.component';

describe('Und3Component', () => {
  let component: Und3Component;
  let fixture: ComponentFixture<Und3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Und3Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Und3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
