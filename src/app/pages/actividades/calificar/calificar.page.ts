import { R3BoundTarget } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotasService } from '../servicios/notas.service';

@Component({
  selector: 'app-calificar',
  templateUrl: './calificar.page.html',
  styleUrls: ['./calificar.page.scss'],
})
export class CalificarPage implements OnInit {

  constructor(private servicio:NotasService, private ruta:Router) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.servicio.respuesta= []; 
  }
    //nota:number = this.servicio.mostrarnota();
    nota:number=this.servicio.mostrarnota();
  
  calificacion():string{
    if(this.nota>3){
      return "assets/images/gato-feliz.jpg";
    }
    else
    {
      return "assets/images/gato-triste.jpg";
    }
  }

  Regresarhome(){
    this.ruta.navigateByUrl("/folder/inicio")
  }

}
