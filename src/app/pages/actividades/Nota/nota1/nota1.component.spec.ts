import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Nota1Component } from './nota1.component';

describe('Nota1Component', () => {
  let component: Nota1Component;
  let fixture: ComponentFixture<Nota1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nota1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Nota1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
