import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Correc2Component } from './correc2.component';

describe('Correc2Component', () => {
  let component: Correc2Component;
  let fixture: ComponentFixture<Correc2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Correc2Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Correc2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
