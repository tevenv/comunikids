import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { respondido } from '../../model/respuesta.models';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-correc2',
  templateUrl: './correc2.component.html',
  styleUrls: ['./correc2.component.scss'],
})
export class Correc2Component implements OnInit {

  public preguntas:cuestionario[]=[
    {
      Titulo:"¿Qué les da Natalia a las yeguas y el burrito para que coman?",
      pregunta1:"Es un perro",
      pregunta2:"Es un gato",
      pregunta3:"Es un pingüino",
      pregunta4:"¡Es un minino muy curioso!",
      id:1
    },
    {
      Titulo:"¿Qué piensa el gato Vladimir cuando ve la nieve a través de la ventana?",
      pregunta1:"Debe estar muy fría y resbaladiza",
      pregunta2:"Es más blanca que mis colmillos",
      pregunta3:"Le encanta ver nevar sobre los tejados",
      pregunta4:"¡ Odio la nieve!",
      id:2
    },
    {
      Titulo:"¿Qué le gusta hacer a los niños cuando cae nieve?",
      pregunta1:"Jugar sobre el parque cubierto de blanco",
      pregunta2:"No salen de casa por el frío",
      pregunta3:"Hacen divertidos y rechonchos muñecos de nieve",
      pregunta4:"Se entretienen observando caer la nieve por la ventana",
      id:3
    },
    {
      Titulo:"¿Qué utilizan los niños para hacer el muñeco de nieve?",
      pregunta1:"Hojas del parque",
      pregunta2:"Piedras redonditas para los ojos",
      pregunta3:"Una fresa para la nariz",
      pregunta4:"Palitos para los brazos",
      id:4
    },
    {
      Titulo:"¿Recuerdas con que prendas visten los niños al muñeco de nieve? ",
      pregunta1:"Un gorro de lana colores",
      pregunta2:"Un sombrero de espía",
      pregunta3:"Una bufanda",
      pregunta4:"Dos guantes",
      id:5
    },
    {
      Titulo:"¿Qué hace cada mañana de invierno el gatito Vladimir?",
      pregunta1:"Se asoma por la ventana",
      pregunta2:"Pasea por el jardín",
      pregunta3:"No hace nada porque tiene sueño",
      pregunta4:"Se acuesta en su cojín junto al radiador",
      id:6
    }
  ];

  public control:contro[]=[{
    control1v: false,
    control1f: true,
    control2v: true,
    control2f: false,
    control3v: false,
    control3f: true,
    control4v: true,
    control4f: false
  },
  {
    control1v: true,
    control1f: false,
    control2v: false,
    control2f: true,
    control3v: true,
    control3f: false,
    control4v: false,
    control4f: true
  },
  {
    control1v: true,
    control1f: false,
    control2v: false,
    control2f: true,
    control3v: true,
    control3f: false,
    control4v: false,
    control4f: true
  },
  {
    control1v: false,
    control1f: true,
    control2v: true,
    control2f: false,
    control3v: false,
    control3f: true,
    control4v: true,
    control4f: false
  },
  {
    control1v: true,
    control1f: false,
    control2v: false,
    control2f: true,
    control3v: true,
    control3f: false,
    control4v: false,
    control4f: true
  },
  {
    control1v: true,
    control1f: false,
    control2v: false,
    control2f: true,
    control3v: false,
    control3f: true,
    control4v: true,
    control4f: false
  }];

  public corregir:cal[]=[{
    calificacion1:"pre1f",
    pregu:1
  },
  {
    calificacion1:"pre2v",
    pregu:1
  },
  {
    calificacion1:"pre3f",
    pregu:1
    
  },
  {
    calificacion1:"pre4v",
    pregu:1
  },
  {
    calificacion1:"pre1v",
    pregu:2
  },
  {
    calificacion1:"pre2f",
    pregu:2
  },
  {
    calificacion1:"pre3v",
    pregu:2
    
  },
  {
    calificacion1:"pre4f",
    pregu:2
  },
  {
    calificacion1:"pre1v",
    pregu:3
  },
  {
    calificacion1:"pre2f",
    pregu:3
  },
  {
    calificacion1:"pre3v",
    pregu:3
    
  },
  {
    calificacion1:"pre4f",
    pregu:3
  },
  {
    calificacion1:"pre1f",
    pregu:4
  },
  {
    calificacion1:"pre2v",
    pregu:4
  },
  {
    calificacion1:"pre3f",
    pregu:4
    
  },
  {
    calificacion1:"pre4v",
    pregu:4
  },
  {
    calificacion1:"pre1v",
    pregu:5
  },
  {
    calificacion1:"pre2f",
    pregu:5
  },
  {
    calificacion1:"pre3v",
    pregu:5
    
  },
  {
    calificacion1:"pre4f",
    pregu:5
  },
  {
    calificacion1:"pre1v",
    pregu:6
  },
  {
    calificacion1:"pre2f",
    pregu:6
  },
  {
    calificacion1:"pre3f",
    pregu:6
    
  },
  {
    calificacion1:"pre4v",
    pregu:6
  }
  ];

  correc:string="Correcto";
  incorr:string="Incorrecto";
  nota:number=0;
  respuesta:respondido[];
  respuestas:cal[]=[];

  constructor(private servicio:NotasService, private ruta:Router) 
  {
    this.respuesta = servicio.mostrar();
    console.log("lo hiciste puesrco",this.respuesta)
  }

  ngOnInit() {}

  calificacion(cod:number):boolean{
    this.respuestas= [];

    for (let index = 0; index < this.respuesta.length; index++) {
      if(this.respuesta[index].pregunta===cod)
      {
        this.respuestas.push({
          calificacion1:this.respuesta[index].respuesta,
          pregu:this.respuesta[index].pregunta
        })
      }
    }

    console.log("funcione perra", this.respuestas)
  
    let aux:number=0;
    let compro:number=0;
    for (let index = 0; index < this.corregir.length; index++) {
      if(this.corregir[index].pregu===cod){
        aux=1+aux;
        for (let i = 0; i < this.respuestas.length; i++) {
          if(this.respuestas[i].calificacion1===this.corregir[index].calificacion1)
          {
            compro=1+compro;
          }
        }
      }
    }

    if(aux===compro){
      this.nota=this.nota+1;
      return true;
    }
    else
    {
      return false;
    }
}

Nota(){
  this.nota=this.nota/10;
  this.servicio.guardarNota(this.nota);
  this.ruta.navigateByUrl("/calificar")
}

}

export interface cuestionario{
  Titulo:string;
  pregunta1:string;
  pregunta2:string;
  pregunta3:string;
  pregunta4:string;
  id:number
}

export interface contro{
  control1v: boolean;
  control1f: boolean;
  control2v: boolean;
  control2f: boolean;
  control3v: boolean;
  control3f: boolean;
  control4v: boolean;
  control4f: boolean;

}

export interface corr{
  pregun1:string;
  pregun2:string;
  pregun3:string;
  pregun4:string;
  id:number;
}

export interface cal{
  calificacion1:string;
  pregu:number;
}
