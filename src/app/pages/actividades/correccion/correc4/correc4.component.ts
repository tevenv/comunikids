import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-correc4',
  templateUrl: './correc4.component.html',
  styleUrls: ['./correc4.component.scss'],
})
export class Correc4Component implements OnInit {

  public respu1:string="Aguacate";
  public respu2:string="Cabello";
  public respu3:string="Pera";
  public respu4:string="Amarilla";
  public respu5:string="Testículo del árbol";
  public respu6:string="México";
  public respu7:string="Nervioso";
  public respu8:string="Tomate";

  correcto1:boolean=false;
  correcto2:boolean=false;
  correcto3:boolean;
  correcto4:boolean;
  correcto5:boolean;
  correcto6:boolean;

  respuesta:string[]=[]
  mota:number=0;

  correc:string="Correcto";
  incorr:string="Incorrecto";

  constructor(private servicio:NotasService, private ruta:Router) 
  {
    this.respuesta = servicio.mostrarabier();
    this.calificacion();
    console.log("Esto es respuesta",this.respuesta[0])
  }

  ngOnInit() {}

  calificacion()
  {
    if(this.respuesta[0] == this.respu1 && this.respuesta[1] == this.respu2 )
    {
      this.correcto1=true;
    }

    if(this.respuesta[2] == this.respu3 && this.respuesta[3] == this.respu4 )
    {
      this.correcto2=true;
    }

    this.correcto3 = this.respuesta[4] == this.respu5 ? true:false;
    this.correcto4 = this.respuesta[5] == this.respu6 ? true:false;
    this.correcto5 = this.respuesta[6] == this.respu7 ? true:false;
    this.correcto6 = this.respuesta[7] == this.respu8 ? true:false;

  }

  Nota()
  {

    if(this.respuesta[0] == this.respu1 && this.respuesta[1] == this.respu2 )
    {
      this.mota= this.mota+1;
    }

    if(this.respuesta[2] == this.respu3 && this.respuesta[3] == this.respu4 )
    {
      this.mota= this.mota+1;
    }

    this.mota = this.respuesta[4] == this.respu5 ? this.mota=this.mota+1:this.mota;
    this.mota = this.respuesta[5] == this.respu6 ? this.mota=this.mota+1:this.mota;
    this.mota = this.respuesta[6] == this.respu7 ? this.mota=this.mota+1:this.mota;
    this.mota = this.respuesta[7] == this.respu8 ? this.mota=this.mota+1:this.mota;

    this.servicio.guardarNota(this.mota);
    this.ruta.navigateByUrl("/calificar")
    
  }

}
