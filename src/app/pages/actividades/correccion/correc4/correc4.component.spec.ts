import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Correc4Component } from './correc4.component';

describe('Correc4Component', () => {
  let component: Correc4Component;
  let fixture: ComponentFixture<Correc4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Correc4Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Correc4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
