import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { strict } from 'assert';
import { respondido } from '../../model/respuesta.models';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-correc3',
  templateUrl: './correc3.component.html',
  styleUrls: ['./correc3.component.scss'],
})
export class Correc3Component implements OnInit {
  
  respuesta:respondido[];
  respuestas:cal[]=[];

  nota:number=0;


  calificar:cal[]=[{
    calificacion1:"pre1",
    pregu:1
  },
  {
    calificacion1:"pre3",
    pregu:1
  },
  {
    calificacion1:"pre1",
    pregu:2
  },
  {
    calificacion1:"pre3",
    pregu:2
  },
  {
    calificacion1:"pre4",
    pregu:2
  },
  {
    calificacion1:"pre2",
    pregu:3
  },
  {
    calificacion1:"pre4",
    pregu:4
  },
  {
    calificacion1:"pre1",
    pregu:5
  },
  {
    calificacion1:"pre2",
    pregu:6
  },

]
  x:boolean;
  correc:string="Correcto";
  incorr:string="Incorrecto";

  public correccion:corr[]=
  [
    {
      cuerpo:"¿Qué les da Natalia a las yeguas y el burrito para que coman?",
      pregunta1:"assets/images/zanahoria.png",
      pregunta2:"assets/images/Pajar.png",
      id:1
    },
    {
      cuerpo:"¿Qué frutas y hortalizas crece en el huerto de los abuelos de Natalia?",
      pregunta1:"assets/images/tomato.png",
      pregunta2:"assets/images/papa.png",
      pregunta3:"assets/images/lechuga.png",
      id:2
    },
    {
      cuerpo:"¿Cuánto tiempo paso la gallina cuidando de los huevos hasta que salieron los pollitos?",
      pregunta1:"assets/images/D3.png",
      correcta:"pre2",
      id:3
    },
    {
      cuerpo:"¿Dónde pone Natalia los huevos que recoge en el gallinero?",
      pregunta1:"assets/images/CUBETA.png",
      correcta:"pre4",
      id:4
    },
    {
      cuerpo:"¿Cuantos años tiene Natalia?",
      pregunta1:"assets/images/10.png",
      correcta:"pre1",
      id:5
    },
    {
      cuerpo:"6.	La niña se asomó a la caseta y junto a la gallina se encontró:",
      pregunta1:"assets/images/pollo.png",
      correcta:"pre2",
      id:6
    }
  ]

  constructor(private servicio:NotasService, private ruta:Router) 
  { 
    this.respuesta = servicio.mostrar();
    console.log("lo hiciste puesrco",this.respuesta)
  }

  ngOnInit() {}
  
  calificacion(cod:number):boolean{
    this.respuestas= [];

    for (let index = 0; index < this.respuesta.length; index++) {
      if(this.respuesta[index].pregunta===cod)
      {
        this.respuestas.push({
          calificacion1:this.respuesta[index].respuesta,
          pregu:this.respuesta[index].pregunta
        })
      }
    }

    console.log("funcione perra", this.respuestas)
  
    let aux:number=0;
    let compro:number=0;
    for (let index = 0; index < this.calificar.length; index++) {
      if(this.calificar[index].pregu===cod){
        aux=1+aux;
        for (let i = 0; i < this.respuestas.length; i++) {
          if(this.respuestas[i].calificacion1===this.calificar[index].calificacion1)
          {
            compro=1+compro;
          }
        }
      }
    }

    if(aux===compro){
      this.nota=this.nota+1;
      return true;
    }
    else
    {
      return false;
    }
}

Nota(){
  this.nota=this.nota/10;
  this.servicio.guardarNota(this.nota);
  this.ruta.navigateByUrl("/calificar")
}

}

export interface corr{
  cuerpo:string;
  pregunta1:string;
  pregunta2?:string;
  pregunta3?:string;
  pregunta4?:string;
  correcta?:string;
  id:number;
}

export interface cal{
  calificacion1:string;
  pregu:number;
}