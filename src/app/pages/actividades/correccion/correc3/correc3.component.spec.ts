import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Correc3Component } from './correc3.component';

describe('Correc3Component', () => {
  let component: Correc3Component;
  let fixture: ComponentFixture<Correc3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Correc3Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Correc3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
