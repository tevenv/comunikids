import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { correcresp } from '../../model/correccion-respues.models';
import { respondido } from '../../model/respuesta.models';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-correc1',
  templateUrl: './correc1.component.html',
  styleUrls: ['./correc1.component.scss'],
})
export class Correc1Component implements OnInit {

  public correccion:correcresp[]=[{
    respuesta:"El pez lloron.",
    pregunta:"¿Cuál es el titulo del cuento?",
    correcta:"pre3",
    pregu:1
  },
  {
    respuesta:"Colorines, kiko, la mamá de Miguelito.",
    pregunta:"¿Cuáles son los personajes del cuento ?",
    correcta:"pre2",
    pregu:2
  },
  {
    respuesta:"El pez.",
    pregunta:"¿Cuál es el personaje principal?",
    correcta:"pre1",
    pregu:3
  },
  {
    respuesta:"Una pecera.",
    pregunta:"¿En qué espacio se desarolla la historia?",
    correcta:"pre3",
    pregu:4
  },
  {
  respuesta:"El pez Colorines vivía feliz y contento con los otros peces de su apartamento. El apartamento era el acuario de unos grandes almacenes. El pez Colorines había nacido allí en una gran pecera.",
  pregunta:"¿Cuál es el inicio del cuento?",
  correcta:"pre1",
  pregu:5
  },
  {
    respuesta:"Colorines.",
    pregunta:"¿Este personaje está muy triste en su nueva casa?",
    correcta:"pre3",
    pregu:6
  }
  ];

  correc:string="Correcta";
  incorr:string="Incorrecta"

  public respuesta:respondido[];
  constructor(private servicio:NotasService, private ruta:Router) {
    this.respuesta = servicio.mostrar();
    console.log("Este es respuesta ", this.respuesta);
    this.llamar();
  }
  
  ngOnInit() {}

  llamar(){
    this.respuesta=this.eliminararreglo();
    console.log("lo logre? ",this.respuesta);
  }
  
  eliminararreglo(){
  
    console.log("arreglo")

    for (let index = 0; index < this.respuesta.length; index++) {
      let aux=index+1;
      let z=0;

      for (let i = aux; i <this.respuesta.length; i++) {
        if(this.respuesta[index].pregunta===this.respuesta[i].pregunta )
        {
        z++;
        }
      }

      for (let i = aux; i <this.respuesta.length; i++) {
        if(this.respuesta[index].pregunta===this.respuesta[i].pregunta )
        {
          this.respuesta.splice(index,z);
          console.log("en el ciclo");
        }
      }
      
    }
    return this.respuesta
  }

  Nota(){
    let nota:number=0;
    for (let index = 0; index < this.respuesta.length; index++) {
      if(this.respuesta[index].respuesta == this.correccion[index].correcta)
      {
        nota = nota+1;
      }
    }
    this.servicio.guardarNota(nota);
    this.ruta.navigateByUrl("/calificar")
  }

}
