import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Correc1Component } from './correc1.component';

describe('Correc1Component', () => {
  let component: Correc1Component;
  let fixture: ComponentFixture<Correc1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Correc1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Correc1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
