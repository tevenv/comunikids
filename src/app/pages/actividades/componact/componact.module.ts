import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lectu1Component } from '../lecturas/lectu1/lectu1.component';
import { IonicModule } from '@ionic/angular';
import { Eva1Component } from '../evaluaciones/eva1/eva1.component';
import { FormsModule } from '@angular/forms';
import { Correc1Component } from '../correccion/correc1/correc1.component';
import { Lectu3Component } from '../lecturas/lectu3/lectu3.component';
import { Eva3Component } from '../evaluaciones/eva3/eva3.component';
import { Correc3Component } from '../correccion/correc3/correc3.component';
import { Lectu4Component } from '../lecturas/lectu4/lectu4.component';
import { Eva4Component } from '../evaluaciones/eva4/eva4.component';
import { Correc4Component } from '../correccion/correc4/correc4.component';
import { Lectu2Component } from '../lecturas/lectu2/lectu2.component';
import { Eva2Component } from '../evaluaciones/eva2/eva2.component';
import { Correc2Component } from '../correccion/correc2/correc2.component';




@NgModule({
  declarations: [
    Lectu1Component,
    Eva1Component,
    Correc1Component,
    Lectu2Component,
    Eva2Component,
    Correc2Component,
    Lectu3Component,
    Eva3Component,
    Correc3Component,
    Lectu4Component,
    Eva4Component,
    Correc4Component
    
  ],
  exports:[
    Lectu1Component,
    Eva1Component,
    Correc1Component,
    Lectu2Component,
    Eva2Component,
    Correc2Component,
    Lectu3Component,
    Eva3Component,
    Correc3Component,
    Lectu4Component,
    Eva4Component,
    Correc4Component
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class ComponactModule { }
