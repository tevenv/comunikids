import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RetroAlimePage } from './retro-alime.page';

const routes: Routes = [
  {
    path: '',
    component: RetroAlimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RetroAlimePageRoutingModule {}
