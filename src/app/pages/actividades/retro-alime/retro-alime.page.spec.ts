import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RetroAlimePage } from './retro-alime.page';

describe('RetroAlimePage', () => {
  let component: RetroAlimePage;
  let fixture: ComponentFixture<RetroAlimePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetroAlimePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RetroAlimePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
