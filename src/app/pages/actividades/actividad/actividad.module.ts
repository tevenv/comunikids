import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadPageRoutingModule } from './actividad-routing.module';

import { ActividadPage } from './actividad.page';
import { ComponactModule } from '../componact/componact.module';
//import { Lectu1Component } from '../lecturas/lectu1/lectu1.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadPageRoutingModule,
    ComponactModule 
   // Lectu1Component
  ],
  declarations: [
    ActividadPage
  ]
})
export class ActividadPageModule {}
