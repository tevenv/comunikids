import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-actividad',
  templateUrl: './actividad.page.html',
  styleUrls: ['./actividad.page.scss'],
})
export class ActividadPage implements OnInit {

  folder:string;
  un1:boolean;
  un2:boolean;
  un3:boolean;
  un4:boolean;

  constructor(private activatedRoute: ActivatedRoute) 
  { }

  ngOnInit() 
  {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    this.lectu1();
    this.lectu2();
    this.lectu3();
    this.lectu4();
  }

  lectu1(){
    if(this.folder=="uni1")
    {
      this.un1=true;
    }
    else
    {
      this.un1=false;
    }
  }

  lectu2(){
    if(this.folder=="uni2")
    {
      this.un2=true;
    }
    else
    {
      this.un2=false;
    }
  }

  lectu3(){
    if(this.folder=="uni3")
    {
      this.un3=true;
    }
    else
    {
      this.un3=false;
    }
  }

  lectu4(){
    if(this.folder=="uni4")
    {
      this.un4=true;
    }
    else
    {
      this.un4=false;
    }
  }

}
