import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Eva4Component } from './eva4.component';

describe('Eva4Component', () => {
  let component: Eva4Component;
  let fixture: ComponentFixture<Eva4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Eva4Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Eva4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
