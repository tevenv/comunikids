import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-eva4',
  templateUrl: './eva4.component.html',
  styleUrls: ['./eva4.component.scss'],
})
export class Eva4Component implements OnInit {
   num:number=0;
  constructor(private servicio:NotasService, private ruta:Router) { }
  arreglo:string[]=[]
  ngOnInit() {}

  Guardar(forma:NgForm){
    this.servicio.guardarabier(this.arregararre(forma));
    this.ruta.navigateByUrl('/retro-alime/uni4')
  }

  arregararre(forma:NgForm):string[]
  {
    this.arreglo[0]=forma.value.pre1;
    this.arreglo[1]=forma.value.pre2;
    this.arreglo[2]=forma.value.pre3;
    this.arreglo[3]=forma.value.pre4;
    this.arreglo[4]=forma.value.pre5;
    this.arreglo[5]=forma.value.pre6;
    this.arreglo[6]=forma.value.pre7;
    this.arreglo[7]=forma.value.pre8;
    return this.arreglo
  }
  
}
