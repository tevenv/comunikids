import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { strict } from 'assert';
import { respondido } from '../../model/respuesta.models';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-eva1',
  templateUrl: './eva1.component.html',
  styleUrls: ['./eva1.component.scss'],
})
export class Eva1Component implements OnInit {

  public Cuestionario:cuestionario[]=[
    {
      cuerpo: "¿Cuál es el título del cuento?",
      pregunta1: "El pez en el mar",
      pregunta2: "El pero y el pez ",
      pregunta3: "El pez llorón " ,
      id:1
    },
    {
      cuerpo: "¿Cuáles son los personajes del cuento ?",
      pregunta1: "Un gato, pez, niño",
      pregunta2: "Colorines, kiko, la mamá de Miguelito",
      pregunta3: "Un pez, la señora y el niño " ,
      id:2
    },
    {
      cuerpo: "¿Cuál es el personaje principal?",
      pregunta1: "El pez",
      pregunta2: "El perro ",
      pregunta3: "El gato " ,
      id:3
    },
    {
      cuerpo: "¿En qué espacio se desarolla la historia?",
      pregunta1: "El apartamento",
      pregunta2: "Un almacen ",
      pregunta3: "Una pecera",
      id:4
    },
    {
      cuerpo: "¿Cuál es el inicio del cuento? ",
      pregunta1: "El pez Colorines vivía feliz y contento con los otros peces de su apartamento. El apartamento era el acuario de unos grandes almacenes. El pez Colorines había nacido allí en una gran pecera.",
      pregunta2: "El pez Colorines no estaba en ninguna cárcel, estaba en una pecera, y estaba en una casa, encima de la chimenea, junto al televisor.",
      pregunta3: "El pez Colorines estaba muy triste y asustado, no sabía estar solo o no quería estar solo." ,
      id:5
    },
    {
      cuerpo: "¿Este personaje está muy triste en su nueva casa?",
      pregunta1: "Kiko",
      pregunta2: "Miguelito",
      pregunta3: "colorines",
      id:6
    }
  ];

  public respuesta:respondido[]=[];

  constructor(public Respuesa:NotasService, private ruta:Router) 
  { 
  }

  ngOnInit() {  }

  guardar(){
    this.Respuesa.guardar(this.respuesta);
    this.ruta.navigateByUrl('/retro-alime/uni1')

  }

  public resultado(resul, algo:number){
    
    console.log(resul.detail.value, algo);

    this.respuesta.push({
      respuesta:resul.detail.value, 
      pregunta:algo
    })

  }

}

export interface cuestionario{
  cuerpo:string;
  pregunta1:string;
  pregunta2:string;
  pregunta3:string;
  id:number
};




