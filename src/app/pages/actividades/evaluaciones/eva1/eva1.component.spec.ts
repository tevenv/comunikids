import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Eva1Component } from './eva1.component';

describe('Eva1Component', () => {
  let component: Eva1Component;
  let fixture: ComponentFixture<Eva1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Eva1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Eva1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
