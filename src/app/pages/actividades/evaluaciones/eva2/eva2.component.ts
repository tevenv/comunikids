import { Component, OnInit } from '@angular/core';
import { ControlContainer } from '@angular/forms';
import { Router } from '@angular/router';
import { respondido } from '../../model/respuesta.models';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-eva2',
  templateUrl: './eva2.component.html',
  styleUrls: ['./eva2.component.scss'],
})
export class Eva2Component implements OnInit {
  
  public preguntas:cuestionario[]=[
    {
      Titulo:"¿Qué es Vladimir?",
      pregunta1:"Es un perro",
      pregunta2:"Es un gato",
      pregunta3:"Es un pingüino",
      pregunta4:"¡Es un minino muy curioso!",
      id:1
    },
    {
      Titulo:"¿Qué piensa el gato Vladimir cuando ve la nieve a través de la ventana?",
      pregunta1:"Debe estar muy fría y resbaladiza",
      pregunta2:"Es más blanca que mis colmillos",
      pregunta3:"Le encanta ver nevar sobre los tejados",
      pregunta4:"¡ Odio la nieve!",
      id:2
    },
    {
      Titulo:"¿Qué le gusta hacer a los niños cuando cae nieve?",
      pregunta1:"Jugar sobre el parque cubierto de blanco",
      pregunta2:"No salen de casa por el frío",
      pregunta3:"Hacen divertidos y rechonchos muñecos de nieve",
      pregunta4:"Se entretienen observando caer la nieve por la ventana",
      id:3
    },
    {
      Titulo:"¿Qué utilizan los niños para hacer el muñeco de nieve?",
      pregunta1:"Hojas del parque",
      pregunta2:"Piedras redonditas para los ojos",
      pregunta3:"Una fresa para la nariz",
      pregunta4:"Palitos para los brazos",
      id:4
    },
    {
      Titulo:"¿Recuerdas con que prendas visten los niños al muñeco de nieve? ",
      pregunta1:"Un gorro de lana colores",
      pregunta2:"Un sombrero de espía",
      pregunta3:"Una bufanda",
      pregunta4:"Dos guantes",
      id:5
    },
    {
      Titulo:"¿Qué hace cada mañana de invierno el gatito Vladimir?",
      pregunta1:"Se asoma por la ventana",
      pregunta2:"Pasea por el jardín",
      pregunta3:"No hace nada porque tiene sueño",
      pregunta4:"Se acuesta en su cojín junto al radiador",
      id:6
    }
  ];

  control:contro[]=[{
    control1v: false,
    control1f: false,
    control2v: false,
    control2f: false,
    control3v: false,
    control3f: false,
    control4v: false,
    control4f: false
  },
  {
    control1v: false,
    control1f: false,
    control2v: false,
    control2f: false,
    control3v: false,
    control3f: false,
    control4v: false,
    control4f: false
  },
  {
    control1v: false,
    control1f: false,
    control2v: false,
    control2f: false,
    control3v: false,
    control3f: false,
    control4v: false,
    control4f: false
  },
  {
    control1v: false,
    control1f: false,
    control2v: false,
    control2f: false,
    control3v: false,
    control3f: false,
    control4v: false,
    control4f: false
  },
  {
    control1v: false,
    control1f: false,
    control2v: false,
    control2f: false,
    control3v: false,
    control3f: false,
    control4v: false,
    control4f: false
  },
  {
    control1v: false,
    control1f: false,
    control2v: false,
    control2f: false,
    control3v: false,
    control3f: false,
    control4v: false,
    control4f: false
  }];

  respuesta:respondido[]=[];

  constructor(public Respuesa:NotasService, private ruta:Router) { }

  ngOnInit() {
  }

  resultado(resul, algo:number, i:number){
    console.log(resul.detail.value, algo, resul.detail.checked, i );
    this.controlar(resul,i);
    this.eliminarrepe(resul, algo);
  }
  controlar(resul, i:number)
  {
    if(resul.detail.value === "pre1v" && resul.detail.checked)
    {
      this.control[i].control1f = false;
    }
    if(resul.detail.value === "pre1f" && resul.detail.checked)
    {
      this.control[i].control1v = false;
    }
  
    if(resul.detail.value === "pre2v" && resul.detail.checked)
    {
      this.control[i].control2f = false;
    }
    if(resul.detail.value === "pre2f" && resul.detail.checked)
    {
      this.control[i].control2v = false;
    }
  
    if(resul.detail.value === "pre3v" && resul.detail.checked)
    {
      this.control[i].control3f = false;
    }
    if(resul.detail.value === "pre3f" && resul.detail.checked)
    {
      this.control[i].control3v = false;
    }
  
    if(resul.detail.value === "pre4v" && resul.detail.checked)
    {
      this.control[i].control4f = false;
    }
    if(resul.detail.value === "pre4f" && resul.detail.checked)
    {
      this.control[i].control4v = false;
    }
  
  }

  eliminarrepe(resul, algo:number){
    if(resul.detail.checked){
      this.respuesta.push({
        respuesta:resul.detail.value, 
        pregunta:algo
      })
    }
    else
    {
      for (let index = 0; index < this.respuesta.length; index++) {
        if(this.respuesta[index].respuesta=== resul.detail.value && this.respuesta[index].pregunta=== algo)
        {
          this.respuesta.splice(index,1);
        }
        
      }
    }
  console.log("arreglo completo? ", this.respuesta);
  }

  guardar(){
    this.Respuesa.guardar(this.respuesta);
    this.ruta.navigateByUrl('/retro-alime/uni2')
  }

}

export interface cuestionario{
  Titulo:string;
  pregunta1:string;
  pregunta2:string;
  pregunta3:string;
  pregunta4:string;
  id:number
}

export interface contro{
  control1v: boolean,
  control1f: boolean,
  control2v: boolean,
  control2f: boolean,
  control3v: boolean,
  control3f: boolean,
  control4v: boolean,
  control4f: boolean,

}
