import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Eva2Component } from './eva2.component';

describe('Eva2Component', () => {
  let component: Eva2Component;
  let fixture: ComponentFixture<Eva2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Eva2Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Eva2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
