import { Component, OnInit } from '@angular/core';
import { Form } from '@angular/forms';
import { Router } from '@angular/router';
import { respondido } from '../../model/respuesta.models';
import { NotasService } from '../../servicios/notas.service';

@Component({
  selector: 'app-eva3',
  templateUrl: './eva3.component.html',
  styleUrls: ['./eva3.component.scss'],
})
export class Eva3Component implements OnInit {

  public cuestionario:cuestionario[]=[
    {
      cuerpo:"¿Qué les da Natalia a las yeguas y el burrito para que coman?",
      pregunta1:"assets/images/zanahoria.png",
      pregunta2:"assets/images/Manzana.png",
      pregunta3:"assets/images/Pajar.png",
      pregunta4:"assets/images/banana.png",
      id:1
    },
    {
      cuerpo:"¿Qué frutas y hortalizas crece en el huerto de los abuelos de Natalia?",
      pregunta1:"assets/images/tomato.png",
      pregunta2:"assets/images/piña.png",
      pregunta3:"assets/images/papa.png",
      pregunta4:"assets/images/lechuga.png",
      id:2
    },
    {
      cuerpo:"¿Cuánto tiempo paso la gallina cuidando de los huevos hasta que salieron los pollitos?",
      pregunta1:"assets/images/D1.png",
      pregunta2:"assets/images/D3.png",
      pregunta3:"assets/images/D2.png",
      pregunta4:"assets/images/D4.png",
      id:3
    },
    {
      cuerpo:"¿Dónde pone Natalia los huevos que recoge en el gallinero?",
      pregunta1:"assets/images/BOX.png",
      pregunta2:"assets/images/CESTA.png",
      pregunta3:"assets/images/bag.png",
      pregunta4:"assets/images/CUBETA.png",
      id:4
    },
    {
      cuerpo:"¿Cuantos años tiene Natalia?",
      pregunta1:"assets/images/10.png",
      pregunta2:"assets/images/6.png",
      pregunta3:"assets/images/8.png",
      pregunta4:"assets/images/9.png",
      id:5
    },
    {
      cuerpo:"6.	La niña se asomó a la caseta y junto a la gallina se encontró:",
      pregunta1:"assets/images/vegeta.png",
      pregunta2:"assets/images/pollo.png",
      pregunta3:"assets/images/huevo.png",
      pregunta4:"assets/images/duck.png",
      id:6
    }
  ]

  estado:boolean= false;

  public respuesta:respondido[]=[];

  constructor(public Respuesa:NotasService, private ruta:Router) { }

  ngOnInit() {}

  public resultado(resul, algo:number){
    console.log(resul.detail.value, algo, resul.detail.checked )
    if(resul.detail.checked){
      this.respuesta.push({
        respuesta:resul.detail.value, 
        pregunta:algo
      })
    }
    else
    {
      for (let index = 0; index < this.respuesta.length; index++) {
        if(this.respuesta[index].respuesta=== resul.detail.value && this.respuesta[index].pregunta=== algo)
        {
          this.respuesta.splice(index,1);
        }
        
      }
    }
  console.log(this.respuesta);
  }

  guardar(){
    this.Respuesa.guardar(this.respuesta);
    this.ruta.navigateByUrl('/retro-alime/uni3')
  }

}

  export interface cuestionario{
    cuerpo:string;
    pregunta1:string;
    pregunta2:string;
    pregunta3:string;
    pregunta4:string;
    id:number
  };

