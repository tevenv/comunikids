import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Eva3Component } from './eva3.component';

describe('Eva3Component', () => {
  let component: Eva3Component;
  let fixture: ComponentFixture<Eva3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Eva3Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Eva3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
