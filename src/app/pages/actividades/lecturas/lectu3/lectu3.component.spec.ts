import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lectu3Component } from './lectu3.component';

describe('Lectu3Component', () => {
  let component: Lectu3Component;
  let fixture: ComponentFixture<Lectu3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lectu3Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lectu3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
