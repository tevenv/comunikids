import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lectu3',
  templateUrl: './lectu3.component.html',
  styleUrls: ['./lectu3.component.scss'],
})
export class Lectu3Component implements OnInit {

  titulo:string="¿Qué le pasa a la gallina?";
  contenido1:string="Natalia tenía 10 años, y desde que tenía 6 pasaba los veranos en la granja de sus abuelos. A Natalia le gustaban mucho los animales y disfrutaba las vacaciones ayudando a sus abuelos a cuidarlos.";
  contenido2:string="Era una granja rodeada de campo para que las vacas y las ovejas pudieran comer y pasear durante el día. Las dos yeguas y el burrito de su abuelo se refugiaban del sol dentro del establo. Había un gallinero repleto de ruidosas gallinas, algunas se escondían a poner sus huevos en una caseta y otras paseaban y picoteaban, también vivía allí un gallo con la cola de colores que les despertaba todas las mañanas. En un lateral de la casa, su abuela y su abuelo habían sembrado lechugas, tomates, zanahorias y unas enormes sandías, y en un extremo crecía fuerte un limonero que sus abuelos habían plantado 6 años antes.";
  contenido3:string="La tarea favorita de Natalia era la de dar alfalfa y zanahorias a los caballos y el burrito, y, aunque el gallo le daba un poco de miedo, era muy valiente y también entraba en el gallinero para recoger los huevos que las gallinas habían puesto y repartía el maíz para que comieran.";
  contenido4:string="Una mañana Natalia entró en el gallinero como de costumbre, repartió el maíz y todas las gallinas y el gallo corrieron a comer. La niña entro en la caseta y empezó a recoger los huevos, poniéndolos con mucho cuidado en un cubo. Entonces observó que una de las gallinas estaba muy quieta en un rincón sobre un montoncito de paja y Natalia se preocupó porque no había ido a comer como el resto ¡quizás estaba enferma!";
  contenido5:string="La niña fue corriendo a llamar a sus abuelos y les explicó lo que pasaba, todos volvieron al gallinero y la vieron. Los abuelos se quedaron más tranquilos ¿qué pasaba? “La gallina está bien, pero no podemos molestarla, sólo tenemos que esperar unas semanas” le dijo su abuela. Pero Natalia no estaba tan tranquila, cada vez que entraba al gallinero encontraba a la gallina allí sentada ¿por qué pasaba tanto tiempo escondida?";
  contenido6:string="Natalia dejó pasar los días y seguía cuidando del resto de animales como siempre hacía. Pasaron todos los días que tuvieron que pasar, hasta que esos días sumaron 3 semanas.";
  contenido7:string="Natalia volvió al gallinero, echó el maíz en las cajitas y todas las gallinas y el gallo corrieron a comer. De nuevo todas las gallinas menos aquella que siempre descansaba en el rincón. Pero aquél día el ruido del gallinero era diferente ¿qué era aquello que escuchaba? La niña se asomó a la caseta y encontró junto a la gallina ¡seis pollitos! Estaban todos juntitos sobre el montón de paja, eran muy pequeños, con plumas finas y amarillas y piaban sin parar.";
  contenido8:string="Como el día que se preocupó por la gallina, Natalia corrió muy contenta a avisar a sus abuelos ¡La gallina había tenido pollitos! ¿Qué había pasado todo ese tiempo? “La gallina ha estado cuidando de los huevos durante 3 semanas, dándoles calor para que dentro de cada huevo creciera un pollito hasta que estuvieran tan grandes y fuertes que pudieran romper el cascarón y salir”. Natalia y sus abuelos los dejaron descansar y al cuidado de su madre el resto del día.";
  contenido9:string="Todos pasaron el verano observando cómo cada día y poco a poco los pollitos crecían, comían y exploraban. ¡Qué verano tan divertido!";
  


  constructor(private ruta:Router) { }

  ngOnInit() {}

  iniexam(){
    this.ruta.navigateByUrl('/examen/uni3');
  }

}
