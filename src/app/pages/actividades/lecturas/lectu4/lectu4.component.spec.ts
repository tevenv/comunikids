import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lectu4Component } from './lectu4.component';

describe('Lectu4Component', () => {
  let component: Lectu4Component;
  let fixture: ComponentFixture<Lectu4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lectu4Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lectu4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
