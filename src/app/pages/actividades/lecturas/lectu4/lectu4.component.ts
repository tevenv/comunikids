import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lectu4',
  templateUrl: './lectu4.component.html',
  styleUrls: ['./lectu4.component.scss'],
})
export class Lectu4Component implements OnInit {

  public titulo:string="El aguacate"

  public contenido1:string="Aguacate es el nombre que recibe el fruto del árbol del aguacate."
  public contenido2:string="Los primeros seres humanos que tuvieron el honor de probarlo fueron los habitantes de México, donde se cultiva desde hace miles de años. Aunque hoy en día existen plantaciones de aguacates por todo el mundo, este país americano sigue siendo el que más cantidad produce."
  public contenido3:string="Son tantos miles de toneladas los que allí se cosechan al año, que una buena parte se vende a otros países. Entre los grandes productores también están Estados Unidos, Indonesia, Brasil, Colombia y España."
  public contenido4:string="La palabra “aguacate” viene del náhuatl, una lengua que hablaban los indígenas en México mucho antes de la llegada de los españoles. Concretamente deriva de la palabra ahuacatl, que significa ‘testículos de árbol’. "
  public contenido5:string="Sí, probablemente escogieron este nombre tan peculiar por el parecido físico que existe entre el fruto y los órganos del aparato reproductor masculino."
  public contenido6:string="El aguacate tiene forma de pera y tres partes bien diferenciadas: una corteza oscura y rugosa para proteger el interior, una pulpa carnosa que puede ser verde o amarilla, y una enorme semilla muy suave al tacto."
  public contenido7:string="Es importante que sepas que el aguacate está considerado uno de los alimentos más saludables que puede comer el ser humano ¿Sabes por qué? Pues porque tiene muchísimas propiedades beneficiosas para el organismo."
  public contenido8:string="Para empezar, posee dos grandes cualidades: es muy nutritivo y rebosa vitaminas. Además, tiene mucho potasio, mineral importantísimo para el correcto funcionamiento de nuestro cuerpo. De hecho, un aguacate aporta el triple de potasio que otra fruta famosa por tener un montón: el plátano."
  public contenido9:string="El aguacate también se distingue por su alto contenido en grasas mono insaturadas, fundamentales durante la etapa de crecimiento porque contribuyen a mejorar el funcionamiento tanto del cerebro como del sistema nervioso. Estas grasas saludables también están presentes en otros alimentos como el aceite de oliva o las almendras."
  public contenido10:string="El aguacate se consume sobre todo en fresco y está buenísimo sazonado o como un ingrediente más de las ensaladas ¡Cuando te prepares una prueba a añadir unos taquitos! Ahora bien, el plato más famoso que tiene al aguacate como protagonista principal, es el guacamole mexicano."
  public contenido11:string="El guacamole es una salsa espesa que se elabora mezclando aguacate con otros productos frescos que básicamente son: tomate, cilantro, cebolla y jugo de lima o de limón. Se suele comer con unas tortillas de maíz que todos conocemos con el nombre de ‘nachos’ o ‘totopos’. Puedes animarte a hacerlo en casa con ayuda de tus padres porque es muy fácil de preparar y no necesita ningún tipo de cocción ¡Si no lo has probado, te encantará!."
  public contenido12:string="Aparte de ser un alimento fantástico, el aguacate también se utiliza para elaborar productos cosméticos gracias a sus aceites, estupendos para hidratar la piel y el cabello."

  constructor(private ruta:Router) { }

  ngOnInit() {}

  iniexam(){
    this.ruta.navigateByUrl('/examen/uni4');
  }

}
