import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lectu1Component } from './lectu1.component';

describe('Lectu1Component', () => {
  let component: Lectu1Component;
  let fixture: ComponentFixture<Lectu1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lectu1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lectu1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
