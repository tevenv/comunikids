import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lectu1',
  templateUrl: './lectu1.component.html',
  styleUrls: ['./lectu1.component.scss'],
})
export class Lectu1Component implements OnInit {

  constructor(private ruta:Router) { }

  ngOnInit() {}

  Titulo:string = "El pez llorón";

  contenido1:string = "El pez Colorines vivía feliz y contento con los otros peces de su apartamento.";
  contenido2:string = "El apartamento era el acuario de unos grandes almacenes. El pez Colorines había nacido allí en una gran pecera. Como no sabía nada de ríos y mares, se creía que el mundo era suyo. Y era feliz dentro del “lago” de agua dulce, encarcelado entre paredes de cristal, con su agua y su comida artificial.";
  contenido3:string = "Y Colorines era feliz, sobre todo porque todos los peces del acuario (de distintos colores, tamaños y precios) eran sus amigos. ";
  contenido4:string = "—¿Cuál quieres, Miguelito? ";
  contenido5:string = "—Ese de colorines tan bonito.";
  contenido6:string = "Y le compraron el pez a Miguelito.";
  contenido7:string = "Colorines se llevó un susto imponente. Por primera vez, el pez se sintió atrapado y rápidamente trasladado a otro lugar. Colorines por poco no se ahoga en el viaje. ";
  contenido8:string = "El otro lugar era una habitación pequeña, redonda, desierta… ";
  contenido9:string = "—Estoy en la cárcel —pensó Colorines—. He oído decir que estar solo es como estar en la cárcel. ";
  contenido10:string = "El pez Colorines no estaba en ninguna cárcel, estaba en una pecera, y estaba en una casa, encima de la chimenea, junto al televisor. ";
  contenido11:string = "Al llegar la noche, todos se acostaron, menos el perro Kiko, que durante horas y horas le observó extrañado. El pez Colorines estaba muy triste y muy asustado. No sabía estar solo o no quería estar solo. El pez Colorines no podía hablar. Se pasó toda la noche llorando. ";
  contenido12:string = "Por la mañana apareció en la sala la madre de Miguelito, se quitó una zapatilla y empezó a regañar al perro Kiko.";
  contenido13:string = "— ¡Sinvergüenza! ¡Cochino! ¡Ven aquí! ¡Maleducado! ¡Hay que ver lo que has hecho! ¿Por qué no dijiste al papá de Miguelín: “papá, pipí”? ";
  contenido14:string = "La señora señalaba con el dedo un gran charco en el suelo. ";
  contenido15:string = "El culpable del gran charco del suelo no fue el perrito Kiko; Kiko no se había hecho pipí. Sucedió que el pez Colorines se pasó toda la noche llorando. Y sus lágrimas aumentaban el agua de la pecera, hasta desbordarse, chimenea abajo.";
  contenido16:string = "Mientras la madre de Miguelito perseguía al perro, Colorines, “el pez llorón”, miraba de reojo la escena, avergonzado, quieto en un rincón de la pecera, sin mover los ojos, sin mover las aletas. Colorines, el pez, no podía hablar. Kiko, el perro, tampoco dijo nada.";

  iniexam(){
    this.ruta.navigateByUrl('/examen/uni1');
  }

}
