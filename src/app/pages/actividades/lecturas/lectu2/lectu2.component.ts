import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lectu2',
  templateUrl: './lectu2.component.html',
  styleUrls: ['./lectu2.component.scss'],
})
export class Lectu2Component implements OnInit {

  Titulo:string = "El gato Vladimir";

  contenido1:string = "Como todas las mañanas de invierno el gatito Vladimir está asomado a la ventana ¡Es un minino muy curioso! Le encanta ver nevar sobre los tejados y a los niños jugando sobre el parque cubierto de blanco.";
  contenido2:string = "A través del cristal escucha sus risas y se entretiene observando cómo hacen divertidos y rechonchos muñecos de nieve.";
  contenido3:string = "Con sus manitas protegidas con guantes de lana, los pequeños forman dos bolas: una grande para el cuerpo y otra más pequeña para la cabeza.";
  contenido4:string = "Después, le ponen botones en la tripa y dos piedras redonditas en el lugar de los ojos. ";
  contenido5:string = "La nariz es una zanahoria larguirucha y dos palitos son los brazos.";
  contenido6:string = "Una niña pelirroja se quita la bufanda y la enrosca en el cuello del pasmado muñeco.";
  contenido7:string = "En su cabeza, ponen un gorro de lana de tres alegres colores ¡Le queda pequeño pero muy gracioso!";
  contenido8:string = "Los niños aplauden cuando ven el resultado. Hacen un corro y dan vueltas alrededor de él mientras cantan canciones.";
  contenido9:string = "Vladimir bosteza y piensa en lo resbaladiza y fría que debe estar esa nieve.";
  contenido10:string = "Se aleja de la ventana y se tumba en su suave y calentito cojín junto al radiador, satisfecho de vivir en una casa tan confortable.";


  constructor( private ruta:Router) { }

  ngOnInit() {}
  iniexam(){
    this.ruta.navigateByUrl('/examen/uni2');
  }

}
