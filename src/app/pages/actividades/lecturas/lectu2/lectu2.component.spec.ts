import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Lectu2Component } from './lectu2.component';

describe('Lectu2Component', () => {
  let component: Lectu2Component;
  let fixture: ComponentFixture<Lectu2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lectu2Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Lectu2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
