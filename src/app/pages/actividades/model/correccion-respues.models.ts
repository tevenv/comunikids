export class correcresp{

    respuesta:string;
    pregunta:string;
    correcta:string;
    pregu:number;

    constructor(Respuesta:string, Pregunta:string, Correcta:string, Pregu:number){
        this.respuesta = Respuesta;
        this.pregunta = Pregunta;
        this.correcta= Correcta;
        this.pregu= Pregu;
    }
}