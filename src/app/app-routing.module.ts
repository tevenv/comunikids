import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/inicio',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'actividad/:id',
    loadChildren: () => import('./pages/actividades/actividad/actividad.module').then( m => m.ActividadPageModule),
      
  },
  {
    path: 'examen/:id',
    loadChildren: () => import('./pages/actividades/examen/examen.module').then( m => m.ExamenPageModule)
  },
  {
    path: 'retro-alime/:id',
    loadChildren: () => import('./pages/actividades/retro-alime/retro-alime.module').then( m => m.RetroAlimePageModule)
  },
  {
    path: 'calificar',
    loadChildren: () => import('./pages/actividades/calificar/calificar.module').then( m => m.CalificarPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
