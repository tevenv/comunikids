import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  public int:boolean;
  public obj:boolean;
  public ini:boolean;
  public un1:boolean;
  public un2:boolean;
  public un3:boolean;
  public un4:boolean;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    this.intro();
    this.objetivo();
    this.inicio();
    this.Un1();
    this.Un2();
    this.Un3();
    this.Un4();
  }

  private intro() {
    if(this.folder=="Introducción"){
      this.int = true;
    }else{
      this.int =false;
    }
  }

  private objetivo() {
    if(this.folder=="Objetivos"){
      this.obj = true;
    }else{
      this.obj =false;
    }
  }

  private inicio() {
    if(this.folder=="inicio"){
      this.ini = true;
    }else{
      this.ini =false;
    }
  }

  private Un1(){
    if(this.folder=="Unidad 1"){
      this.un1 = true;
    }else{
      this.un1 =false;
    }
  }

  private Un2(){
    if(this.folder=="Unidad 2"){
      this.un2 = true;
    }else{
      this.un2 =false;
    }
  }

  private Un3(){
    if(this.folder=="Unidad 3"){
      this.un3 = true;
    }else{
      this.un3 =false;
    }
  }

  private Un4(){
    if(this.folder=="Unidad 4"){
      this.un4 = true;
    }else{
      this.un4 =false;
    }
  }


}





